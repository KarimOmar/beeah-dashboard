'use strict';

/* Controllers */
  // Mainopage controller
app.controller('ViewPollController', ['$scope', '$rootScope', 'Restangular', '$stateParams',
  function($scope, $rootScope, Restangular, $stateParams) {
    var pollId = $stateParams.id;
    var args = {
      userIdToken: $rootScope.user.userIdToken
    };
    Object.size = function(obj) {
      var size = 0, key;
      for (key in obj) {
          if (obj.hasOwnProperty(key)) size++;
      }
      return size;
    };
    Restangular.one('api/v1/polls', pollId).get(args).then(function(res) {
      $scope.poll = res;
      $scope.poll.key = pollId;
      $scope.optionsList = [];
      console.log($scope.poll, Object.size($scope.poll.pollAnswer.choices.c3));
      _.each($scope.poll.pollOptions, function(option, x){
        var optionObj = {};
        optionObj.id = x;
        optionObj.key = optionObj.id.substr(1);
        optionObj.label = 'Choice ' + optionObj.key;
        optionObj.text = option.text;
        optionObj.nAnswers = Object.size($scope.poll.pollAnswer.choices[optionObj.id]) | 0;
        optionObj.nAnswersPercentage = optionObj.nAnswers / $scope.poll.responsesNo * 100;
        optionObj.nAnswersPercentageFormat = optionObj.nAnswersPercentage + '%';
        $scope.optionsList.push(optionObj);
      });
    });
  }
]);

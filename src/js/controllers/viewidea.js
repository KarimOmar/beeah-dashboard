'use strict';

/* Controllers */
  // Mainopage controller
app.controller('ViewIdeaController', ['$scope', '$rootScope', 'Restangular', '$stateParams',
  function($scope, $rootScope, Restangular, $stateParams) {
    var ideaId = $stateParams.id;
    var args = {
      userIdToken: $rootScope.user.userIdToken
    };
    Restangular.one('api/v1/ideas', ideaId).get(args).then(function(res) {
      $scope.idea = res;
      $scope.idea.key = ideaId;
    });
  }
]);

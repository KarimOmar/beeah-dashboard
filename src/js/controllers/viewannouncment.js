'use strict';

/* Controllers */
  // Mainopage controller
app.controller('ViewAnnouncmentController', ['$scope', '$rootScope', 'Restangular', '$stateParams',
  function($scope, $rootScope, Restangular, $stateParams) {
    var announcmentId = $stateParams.id;
    var args = {
      userIdToken: $rootScope.user.userIdToken
    };
    Restangular.one('api/v1/announcements', announcmentId).get(args).then(function(res) {
      $scope.announcment = res;
      $scope.announcment.key = announcmentId;
    });
  }
]);

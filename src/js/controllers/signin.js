'use strict';

/* Controllers */
  // signin controller
app.controller('SigninFormController', ['$scope', '$rootScope', '$http', '$state', '$firebaseAuth', '$window',
  function($scope, $rootScope, $http, $state, $firebaseAuth, $window) {
  $rootScope.user = {};
  $scope.googleLogin = function() {
    $rootScope.userAuth = $firebaseAuth();
    // login with google
    $rootScope.userAuth.$signInWithPopup("google").then(function(firebaseUser) {
      var loggedinUser = {
        name: firebaseUser.user.displayName,
        email: firebaseUser.user.email,
        photo: firebaseUser.user.photoURL,
        uId: firebaseUser.user.uid,
        uAccessToken: firebaseUser.credential.accessToken,
        uIdToken: firebaseUser.credential.idToken,
        userIdToken: firebaseUser.user.ie
      }
      // $rootScope.userAuth = auth;
      $rootScope.user = loggedinUser;
      $rootScope.masterUser = firebaseUser;
      $window.sessionStorage.setItem('user', JSON.stringify(loggedinUser));
      console.log($rootScope.userAuth);
      $state.go("app.landing");
    }).catch(function(error) {
      console.log("Authentication failed:", error);
    });
  };
}]);

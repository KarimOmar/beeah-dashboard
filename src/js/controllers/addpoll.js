'use strict';

/* Controllers */
  // Mainopage controller
app.controller('AddAnnouncmentController', ['$scope', '$rootScope', 'Restangular', '$state', 'firebase',
  function($scope, $rootScope, Restangular, $state, firebase) {
    $scope.model = {};
    var CreatePoll = Restangular.all('api/v1/polls');

    $scope.onSuccess =  function onSuccess(result) {
      $state.go('app.poll');
    }

    $scope.onError =  function onError(error) {
      console.log(error);
    }

    $scope.questionTypes = ['Text', 'Radio', 'Checkbox'];
    $scope.questionType = $scope.questionTypes[1];
    $scope.questionOptionsCount = 0;

    $scope.$watch('questionType', function() {
      if ($scope.questionType.toLowerCase() === 'radio') {
        $scope.questionOptionsCount = 2;
        $scope.questionOptions = [
          {key: 1, id: 'c1', label: 'Choice 1', text: ''},
          {key: 2, id: 'c2', label: 'Choice 2', text: ''}
        ];
      }
    });

    $scope.addChoice = function addChoice() {
      var key = $scope.questionOptions.length + 1;
      var choice = {
        key: key,
        id: 'c' + key,
        label: 'Choice ' + key,
        text: ''
      };
      $scope.questionOptions.push(choice);
      $scope.questionOptionsCount++;
    };

    function formatOptions(optionsList) {
      optionsList = _.groupBy(optionsList, 'id');
      _.each(optionsList, function(option, x) {
        optionsList[x] = {text: option[0].text};
      });
      return optionsList;
    }

    function optionsChecker(optionsList) {
      var checker = 0;
      _.each(optionsList, function(option) {
        if (_.isEmpty(option.text)) {
          checker++;
        }
       });
      return (checker > 0) ? false:true;
    }

    $scope.save = function save() {
      if (!this.questionText || !this.expirationDate || !optionsChecker(this.questionOptions)) {
      // TODO: Add Error Message
      } else {
        var values = {
          questionText: this.questionText,
          pollType: this.questionType.toLowerCase(),
          expirationDate: moment(this.expirationDate, "D/M/YYYY HH:mm").valueOf(),
          published: this.published,
          options: formatOptions(this.questionOptions)
        };
        var userIdToken = $rootScope.user.userIdToken;
        CreatePoll.post(values, {userIdToken: userIdToken}).then(this.onSuccess, this.onError);
      }
    };

  }

]);

'use strict';

/* Controllers */
  // PollPage controller
app.controller('PollController', ['$scope', '$rootScope', 'Restangular',
  function($scope, $rootScope, Restangular) {
    var Polls = Restangular.all('api/v1/polls');
    var args = {
      userIdToken: $rootScope.user.userIdToken
    };
    Polls.getList(args).then(function(response) {
      $scope.polls = response[0].items;
    });
  }
]);

'use strict';

/* Controllers */
  // Mainopage controller
app.controller('AnnouncmentController', ['$scope', '$rootScope', 'Restangular', '$state',
  function($scope, $rootScope, Restangular, $state) {
    var announcment = Restangular.all('api/v1/announcements');
    var args = {
      userIdToken: $rootScope.user.userIdToken
    };
    announcment.getList(args).then(function(response) {
      $scope.announcments = response[0].announcments;
    });

    $scope.deleteAnnouncment = function(announcment) {
      Restangular.one("api/v1/announcements", announcment.id).remove(args).then(function(res) {
        $state.reload();
      });
    };
  }
]);

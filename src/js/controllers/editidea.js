'use strict';

/* Controllers */
  // Mainopage controller
app.controller('EditIdeaController', ['$scope', '$rootScope', 'Restangular', '$stateParams', '$state',
  function($scope, $rootScope, Restangular, $stateParams, $state) {
    var ideaId = $stateParams.id;
    var args = {
      userIdToken: $rootScope.user.userIdToken
    };
    $scope.onSuccess = function onSuccess(res) {
      $scope.idea = res;
      $scope.idea.key = ideaId;
      $scope.title = $scope.idea.title;
      $scope.description = $scope.idea.description;
      console.log(res, $scope);
    }
    Restangular.one('api/v1/ideas', ideaId).get(args).then($scope.onSuccess, _.noop);

    $scope.onSuccessEdit = function onSuccessEdit(res) {
      $state.go('app.idea');
    }

    $scope.update = function save() {
      var values = {
        title: this.title,
        description: this.description
      };
      Restangular.one("api/v1/ideas", ideaId).post('', values, args).then($scope.onSuccessEdit, _.noop);
    };
  }
]);

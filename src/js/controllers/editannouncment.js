'use strict';

/* Controllers */
  // Mainopage controller
app.controller('EditAnnouncmentController', ['$scope', '$rootScope', 'Restangular', '$stateParams', '$state',
  function($scope, $rootScope, Restangular, $stateParams, $state) {
    var announcmentId = $stateParams.id;
    var args = {
      userIdToken: $rootScope.user.userIdToken
    };
    $scope.onSuccess = function onSuccess(res) {
      $scope.announcment = res;
      $scope.announcment.key = announcmentId;
      $scope.title = $scope.announcment.title;
      $scope.text = $scope.announcment.text;
    }
    Restangular.one('api/v1/announcements', announcmentId).get(args).then($scope.onSuccess, _.noop);

    $scope.onSuccessEdit = function onSuccessEdit(res) {
      $state.go('app.announcment');
    }

    $scope.update = function save() {
      var values = {
        title: this.title,
        text: this.text
      };
      Restangular.one("api/v1/announcements", announcmentId).post('', values, args).then($scope.onSuccessEdit, _.noop);
    };
  }
]);

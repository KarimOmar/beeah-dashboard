'use strict';

/* Controllers */
  // Mainopage controller
app.controller('IdeaController', ['$scope', '$rootScope', 'Restangular', '$state',
  function($scope, $rootScope, Restangular, $state) {
    var Idea = Restangular.all('api/v1/ideas');
    var args = {
      userIdToken: $rootScope.user.userIdToken
    };

    Idea.getList(args).then(function(response) {
      $scope.ideas = response[0].items;
    });

    $scope.deleteIdea = function(idea) {
      Restangular.one("api/v1/ideas", idea.ideaKey).remove(args).then(function(res) {
        $state.reload();
      });
    };
  }
]);

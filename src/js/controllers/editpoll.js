'use strict';

/* Controllers */
  // Mainopage controller
app.controller('EditPollController', ['$scope', '$rootScope', 'Restangular', '$stateParams', '$state',
  function($scope, $rootScope, Restangular, $stateParams, $state) {
    var pollId = $stateParams.id;
    var args = {
      userIdToken: $rootScope.user.userIdToken
    };
    $scope.questionTypes = ['Text', 'Radio', 'Checkbox'];
    $scope.onSuccess = function onSuccess(res) {
      $scope.poll = res;
      $scope.poll.key = pollId;
      $scope.questionText = $scope.poll.questionText;
      $scope.published = $scope.poll.published;
      _.each($scope.questionTypes, function(type) {
        var lowerType = type.toLowerCase();
        if (lowerType === $scope.poll.pollType) {
          $scope.questionType = type;
        }
      });
      $scope.questionOptions = [];
      $scope.expirationDate = moment($scope.poll.expirationDate).format("D/M/YYYY HH:mm");
      _.each($scope.poll.pollOptions, function(option, x) {
        var optionObj = {
          id: x,
          key: x.substr(1),
          text: option.text,
          label: 'Choice ' + x.substr(1)
        };
        $scope.questionOptions.push(optionObj);
      });
    }
    Restangular.one('api/v1/polls', pollId).get(args).then($scope.onSuccess, _.noop);

    $scope.$watch('questionType', function() {
      if ($scope.questionType && $scope.questionType.toLowerCase() === 'radio') {
        $scope.questionOptionsCount = $scope.poll.pollOptions.length;
      }
    });

    $scope.onSuccessEdit = function onSuccessEdit(res) {
      $state.go('app.poll');
    }

    $scope.addChoice = function addChoice() {
      var key = $scope.questionOptions.length + 1;
      var choice = {
        key: key,
        id: 'c' + key,
        label: 'Choice ' + key,
        text: ''
      };
      $scope.questionOptions.push(choice);
      $scope.questionOptionsCount++;
    };

    function formatOptions(optionsList) {
      optionsList = _.groupBy(optionsList, 'id');
      _.each(optionsList, function(option, x) {
        optionsList[x] = {text: option[0].text};
      });
      return optionsList;
    }

    function optionsChecker(optionsList) {
      var checker = 0;
      _.each(optionsList, function(option) {
        if (_.isEmpty(option.text)) {
          checker++;
        }
       });
      return (checker > 0) ? false:true;
    }

    $scope.update = function save() {
      if (!this.questionText || !this.expirationDate || !optionsChecker(this.questionOptions)) {
      // TODO: Add Error Message
      } else {
        var values = {
          questionText: this.questionText,
          pollType: this.questionType.toLowerCase(),
          expirationDate: moment(this.expirationDate, "D/M/YYYY HH:mm").valueOf(),
          published: this.published,
          options: formatOptions(this.questionOptions)
        };
        Restangular.one("api/v1/polls", pollId).post('', values, args).then($scope.onSuccessEdit, _.noop);
      }
    };
  }
]);

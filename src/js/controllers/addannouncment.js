'use strict';

/* Controllers */
  // Mainopage controller
app.controller('AddAnnouncmentController', ['$scope', '$rootScope', 'Restangular', '$state', 'firebase',
  function($scope, $rootScope, Restangular, $state, firebase) {
    $scope.model = {};
    var CreateAnnouncment = Restangular.all('api/v1/announcements');

    $scope.onSuccess =  function onSuccess(result) {
      $state.go('app.announcment');
    }

    $scope.onError =  function onError(error) {
      console.log(error);
    }

    $scope.save = function save() {
      var values = {
        title: this.title,
        text: this.text,
        userIdToken: $rootScope.user.userIdToken
      };
      CreateAnnouncment.post('announcment', values).then(this.onSuccess, this.onError);
    };

  }

]);

'use strict';

/* Controllers */
  // Mainopage controller
app.controller('AddIdeaController', ['$scope', '$rootScope', 'Restangular', '$state', 'firebase',
  function($scope, $rootScope, Restangular, $state, firebase) {
    $scope.model = {};
    var PostIdea = Restangular.all('idea/postIdea');

    $scope.onSuccess =  function onSuccess(result) {
      $state.go('app.idea');
    }

    $scope.onError =  function onError(error) {
      console.log(error);
    }

    $scope.save = function save() {
      var values = {
        title: this.title,
        description: this.description,
        userIdToken: $rootScope.user.userIdToken
      };
      PostIdea.post('idea', values).then(this.onSuccess, this.onError);
    };

  }

]);
